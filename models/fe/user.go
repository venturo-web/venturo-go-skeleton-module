package fe

import (
	"encoding/json"
	"fmt"
	"go-skeleton-module/helpers"
	"go-skeleton-module/models"
	"go-skeleton-module/structs"
	"log"
	"time"
)

// GetListUser is for get list user
/**
 * @author Mahendra Dwi Purwanto
 *
 * @param req structs.GetRequestParams
 *
 * @return map[string]interface{}, error
 */
func GetListUser(req structs.GetRequestParams) (map[string]interface{}, error) {
	/* set result struct */
	var result []structs.User

	/* Connect DB dan select tabel user */
	tx := models.ModelsDB.Table("user")

	/* Dynamic filter */
	var filter map[string]interface{}

	if req.Filter != "" {
		err := json.Unmarshal([]byte(req.Filter), &filter)
		if err != nil {
			return nil, err
		}

		for index, element := range filter {
			status, key := helpers.RemoveCharacters(index)
			value := fmt.Sprintf("%v", element)
			if !status {
				log.Println("Error remove characters")
				return nil, fmt.Errorf("internal server error")
			}

			tx.Where(key+" LIKE ?", "%"+value+"%")
		}
	}
	tx.Where("DeletedAt IS NULL")

	/* Limit, Offset, and Count */
	var count int64
	tx.Count(&count)

	if req.Limit > 0 {
		tx.Limit(int(req.Limit))
	}
	if req.Offset > 0 {
		tx.Offset(int(req.Offset))
	}
	tx.Find(&result)

	/* Build response */
	var res map[string]interface{}
	if len(result) == 0 {
		res = map[string]interface{}{
			"list":        nil,
			"total_items": count,
		}
	} else {
		res = map[string]interface{}{
			"list":        result,
			"total_items": count,
		}
	}
	return res, tx.Error
}

// GetDetailUser is for get detail user
/**
 * @author Mahendra Dwi Purwanto
 *
 * @param id int
 *
 * @return structs.User, error
 */
func GetDetailUser(id int) (structs.User, error) {
	var record structs.User

	tx := models.ModelsDB.Table("user").Where("Id", id).Find(&record)
	return record, tx.Error
}

// CreateUser is for create user
/**
 * @author Mahendra Dwi Purwanto
 *
 * @param req structs.CreateUserCategory
 *
 * @return structs.CreateUserCategory, error
 */
func CreateUser(req structs.CreateUser) (structs.CreateUser, error) {
	create := structs.CreateUser{
		Name:      req.Name,
		Foto:      req.Foto,
		Email:     req.Email,
		Password:  req.Password,
		CreatedAt: time.Now().Format("2006-01-02 15:04:05"),
	}
	tx := models.ModelsDB.Table("user").Create(&create)

	return create, tx.Error
}

// UpdateUser is for update user
/**
 * @author Mahendra Dwi Purwanto
 *
 * @param req structs.UpdateUserCategory
 *
 * @return map[string]interface{}, error
 */
func UpdateUser(req structs.User) (map[string]interface{}, error) {
	/* Convert struct ke map[string]interface{} */
	data := helpers.ConvertStructToMap(req, "update")

	/* Connect to DB, select tabel Example dan Update data id yang di request dengan map[string]interface{} yang dibuat di atas */
	tx := models.ModelsDB.Table("user").Where("Id", req.Id).Updates(data)

	return data, tx.Error
}

// DeleteUser is for delete user
/**
 * @author Mahendra Dwi Purwanto
 *
 * @param id int
 *
 * @return error
 */
func DeleteUser(id int) error {
	tNow := time.Now()
	tx := models.ModelsDB.Table("user").Where("Id", id).Update("DeletedAt", tNow.Format("2006-01-02 15:04:05"))
	return tx.Error
}
