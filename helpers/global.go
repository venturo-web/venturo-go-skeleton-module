package helpers

import (
	"encoding/base64"
	"encoding/json"
	"log"
	"math/rand"
	"reflect"
	"regexp"
	"strings"
	"time"
)

// RemoveCharacters is a function to remove all characters except alphanumeric
func RemoveCharacters(str string) (status bool, result string) {

	re, err := regexp.Compile(`[^\w]`)
	if err != nil {
		status = false
		result = ""
	}

	result = strings.TrimSpace(re.ReplaceAllString(str, ""))
	status = true
	return
}

// RemoveCharactersOnly is a function to remove all characters except alphanumeric and space
func RemoveCharactersOnly(str string) (status bool, result string) {
	re, err := regexp.Compile(`[^\w\s]`)
	if err != nil {
		status = false
		result = ""
	}

	result = strings.TrimSpace(re.ReplaceAllString(str, ""))
	status = true

	return
}

// MarshalUnmarshal is a function to marshal and unmarshal data
func MarshalUnmarshal(param interface{}, result interface{}) error {
	paramByte, err := json.Marshal(param)
	if err != nil {
		log.Println("Error marshal", err.Error())
		return err
	}

	err = json.Unmarshal(paramByte, &result)
	if err != nil {
		log.Println("Error unmarshal", err.Error())
		return err
	}

	return nil
}

// SliceStringContains is a function to check if a string is in a slice
func SliceStringContains(sl []string, name string) bool {
	// iterate over the array and compare given string to each element
	for _, value := range sl {
		if value == name {
			return true
		}
	}
	return false
}

// SliceIntContains is a function to check if an int is in a slice
func SliceIntContains(sl []int, name int) bool {
	// iterate over the array and compare given string to each element
	for _, value := range sl {
		if value == name {
			return true
		}
	}
	return false
}

// GenerateRandInt is a function to generate random int
func GenerateRandInt() int64 {
	y1 := rand.New(rand.NewSource(time.Now().UnixNano()))
	id := y1.Int63n(10000)
	return id
}

// DeleteKeyMap is a function to delete key from map
func DeleteKeyMap(data map[string]interface{}, keys []string) map[string]interface{} {
	for _, val := range keys {
		delete(data, val)
	}

	return data
}

// ConvertStructToMap is a function to convert struct to map
func ConvertStructToMap(input interface{}, reqType string) map[string]interface{} {
	data := make(map[string]interface{})

	value := reflect.ValueOf(input)
	typ := reflect.TypeOf(input)

	for i := 0; i < value.NumField(); i++ {
		fieldName := typ.Field(i).Name
		fieldValue := value.Field(i).Interface()

		// Check if the field is a string or int and empty or zero
		switch typ.Field(i).Type.Kind() {
		case reflect.String:
			if fieldValue.(string) == "" {
				fieldValue = nil
			}
		case reflect.Int:
			if fieldValue.(int) == 0 {
				fieldValue = nil
			}
		}

		data[fieldName] = fieldValue
	}

	// check if value is nil, then remove it from the map
	for k, v := range data {
		if v == nil {
			delete(data, k)
		}
	}

	if reqType == "update" {
		// Add other fields as needed
		data["UpdatedAt"] = time.Now().Format("2006-01-02 15:04:05")
	}
	return data
}

// RandomByte is a function to generate random byte
func RandomByte(n int) string {
	b := make([]byte, n)
	_, _ = rand.Read(b)

	return base64.URLEncoding.EncodeToString(b)
}
