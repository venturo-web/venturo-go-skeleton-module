package structs

import "time"

type GetRequestParams struct {
	Limit  int
	Offset int
	Filter string
}

type ExampleStruct struct {
	Id        int64      `json:"Id" gorm:"column:Id;primaryKey"`
	Name      string     `json:"Name,omitempty" gorm:"column:Name"`
	Status    int32      `json:"Status,omitempty" gorm:"column:Status"`
	CreatedAt time.Time  `json:"CreatedAt" gorm:"column:CreatedAt;autoCreateTime"`
	UpdatedAt *time.Time `json:"UpdatedAt" gorm:"column:UpdatedAt;autoUpdateTime"`
	DeletedAt *time.Time `json:"DeletedAt" gorm:"column:DeletedAt"`
}

func (ExampleStruct) TableName() string {
	return "ExampleTable"
}
