package structs

type CreateUser struct {
	Name      string `json:"name" gorm:"column:Name;default:null"`
	Foto      string `json:"foto" gorm:"column:Foto;default:null"`
	Email     string `json:"email" gorm:"column:Email"`
	Password  string `json:"password" gorm:"column:Password;default:null"`
	CreatedAt string `json:"created_at" gorm:"column:CreatedAt;default:null"`
}

type User struct {
	Id        int    `form:"id" gorm:"Id"`
	Name      string `form:"name" gorm:"Name"`
	Foto      string `form:"foto" gorm:"Foto"`
	Email     string `form:"email" gorm:"Email"`
	Password  string `form:"password" gorm:"Password"`
	CreatedAt string `json:"created_at" gorm:"column:CreatedAt"`
	CreatedBy int    `json:"created_by" gorm:"column:CreatedBy"`
	UpdatedAt string `json:"updated_at" gorm:"column:UpdatedAt"`
	UpdatedBy int    `json:"updated_by" gorm:"column:UpdatedBy"`
	DeletedAt string `json:"deleted_at" gorm:"column:DeletedAt"`
}
