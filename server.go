package main

import (
	"go-skeleton-module/helpers"
	"go-skeleton-module/models"
	"go-skeleton-module/routes"
	"log"
	"os"

	"github.com/joho/godotenv"
	"gorm.io/gorm"
)

func main() {
	// triger jenkins
	errLoadingEnvFile := godotenv.Load()
	if errLoadingEnvFile != nil {
		helpers.HandleError("error loading the .env file", errLoadingEnvFile)
	}

	/* connect databse */
	models.ModelsDB = models.ConnectDb(1)

	/* close database */
	connections := []gorm.DB{*models.ModelsDB}
	for _, connection := range connections {
		if db, err := connection.DB(); err != nil {
			log.Println("Error close databse", err.Error())
		} else {
			defer db.Close()
		}
	}

	log.Println("Running the " + os.Getenv("APP_NAME") + ".")

	// consumer rabbitmq
	routes.RabbitMQ(os.Getenv("RQ_QUEUE"))
}
