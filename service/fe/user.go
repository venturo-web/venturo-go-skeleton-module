package fe

import (
	"go-skeleton-module/helpers"
	models "go-skeleton-module/models/fe"
	"go-skeleton-module/structs"
	"net/http"
	"time"
)

func GetListUser(data structs.Request) string {
	id := helpers.GenerateRandInt()

	/* Bind Request data to Struct */
	var req structs.GetRequestParams
	err := helpers.MarshalUnmarshal(data.Data, &req)
	if err != nil {
		return helpers.HandleJSONResponse(id, data.ID, http.StatusBadRequest, "Failed", "", time.Now(), nil)
	}

	/* Pass Request to Models */
	res, err := models.GetListUser(req)
	if err != nil {
		return helpers.HandleJSONResponse(id, data.ID, http.StatusInternalServerError, err.Error(), "", time.Now(), nil)
	}

	return helpers.HandleJSONResponse(id, data.ID, http.StatusOK, "Success", "", time.Now(), res)
}

func GetDetailUser(data structs.Request) string {
	id := helpers.GenerateRandInt()

	/* Bind Request data to Struct */
	var idUpdate = int(data.Data.(map[string]interface{})["id"].(float64))

	/* Pass Request to Models */
	res, err := models.GetDetailUser(idUpdate)
	if err != nil {
		return helpers.HandleJSONResponse(id, data.ID, http.StatusInternalServerError, err.Error(), "", time.Now(), nil)
	}

	return helpers.HandleJSONResponse(id, data.ID, http.StatusOK, "Success", "", time.Now(), res)
}
func CreateUser(data structs.Request) string {
	id := helpers.GenerateRandInt()

	/* Bind Request data to Struct */
	var req structs.CreateUser
	err := helpers.MarshalUnmarshal(data.Data, &req)
	if err != nil {
		return helpers.HandleJSONResponse(id, data.ID, http.StatusBadRequest, "Failed", "", time.Now(), nil)
	}

	/* Pass Request to Models */
	res, err := models.CreateUser(req)
	if err != nil {
		return helpers.HandleJSONResponse(id, data.ID, http.StatusInternalServerError, err.Error(), "", time.Now(), nil)
	}

	return helpers.HandleJSONResponse(id, data.ID, http.StatusOK, "Success", "", time.Now(), res)
}
func UpdateUser(data structs.Request) string {
	id := helpers.GenerateRandInt()

	/* Bind Request data to Struct */
	var req structs.User
	err := helpers.MarshalUnmarshal(data.Data, &req)
	if err != nil {
		return helpers.HandleJSONResponse(id, data.ID, http.StatusBadRequest, "Failed", "", time.Now(), nil)
	}

	/* Pass Request to Models */
	res, err := models.UpdateUser(req)
	if err != nil {
		return helpers.HandleJSONResponse(id, data.ID, http.StatusInternalServerError, err.Error(), "", time.Now(), nil)
	}

	return helpers.HandleJSONResponse(id, data.ID, http.StatusOK, "Success", "", time.Now(), res)
}
func DeleteUser(data structs.Request) string {
	id := helpers.GenerateRandInt()

	/* Bind Request data to Struct */
	var idDelete = int(data.Data.(map[string]interface{})["id"].(float64))

	/* Pass Request to Models */
	err := models.DeleteUser(idDelete)
	if err != nil {
		return helpers.HandleJSONResponse(id, data.ID, http.StatusInternalServerError, err.Error(), "", time.Now(), nil)
	}

	return helpers.HandleJSONResponse(id, data.ID, http.StatusOK, "Success", "", time.Now(), nil)
}
