package routes

import (
	"go-skeleton-module/helpers"
	"go-skeleton-module/service/fe"
	"go-skeleton-module/structs"
	"log"
	"time"
)

const (
	/* route */
	GetList   = "get-list-user"
	GetDetail = "get-detail-user"
	Create    = "create-user"
	Update    = "update-user"
	Delete    = "delete-user"
)

// function for handle command request
func handleRequest(data structs.Request) string {
	log.Println("Accepted command -> ", data.Command)

	// sample command
	switch data.Command {
	case GetList:
		return fe.GetListUser(data)
	case GetDetail:
		return fe.GetDetailUser(data)
	case Create:
		return fe.CreateUser(data)
	case Update:
		return fe.UpdateUser(data)
	case Delete:
		return fe.DeleteUser(data)
	default:
		return helpers.HandleJSONResponse(0, data.ID, 0000, "Failed", "command not found", time.Now(), nil)
	}

}
